import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthGuard from './auth-guard'
import Home from '@/views/Home'
import Orders from '@/views/User/Orders'
import Login from '@/views/Auth/Login'
import Registration from '@/views/Auth/Registration'
import Ad from '@/views/Ads/Ad'
import List from '@/views/Ads/List'
import New from '@/views/Ads/New'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    name: 'home',
    component: Home,
  },
  {
    path: '/orders',
    name: 'orders',
    component: Orders,
    beforeEnter: AuthGuard
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/registration',
    name: 'registration',
    component: Registration,
  },
  {
    path: '/ad/:id',
    props: true,
    name: 'ad',
    component: Ad,
    beforeEnter: AuthGuard
  },
  {
    path: '/list',
    name: 'list',
    component: List,
    beforeEnter: AuthGuard
  },
  {
    path: '/new',
    name: 'new',
    component: New,
    beforeEnter: AuthGuard
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})
console.log(router)
export default router
